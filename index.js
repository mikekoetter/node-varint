'use strict';  
// Requiring the add-on that we'll build in a moment...
var varint = require('./build/Release/varint');

module.exports = {

	GetVarint64 : function(read_buffer, size , write_buffer){

		return varint.GetVarint64(read_buffer, size, write_buffer);

	},
	PutVarint64 : function(write_buffer, toEncode){
		if(write_buffer.length <= 0 ) return 0;
		return varint.PutVarint64(write_buffer, toEncode);

	},
	toNumber : function(buffer){
		var num;
		switch(buffer.length){
        	case 1 : num =buffer.readInt8(0); break;
        	case 2 : num =buffer.readInt16LE(0); break;
        	case 3 : num = Buffer.concat([buffer,new Buffer("00","hex")]).readInt32LE(0); break;
        	case 4 : num = buffer.readInt32LE(0); break;
        	case 5 : num = readInt64LEasFloat(Buffer.concat([buffer,new Buffer("000000","hex")]));break;
        	case 6 : num = readInt64LEasFloat(Buffer.concat([buffer,new Buffer("0000","hex")]));break;
        	case 7 : num = readInt64LEasFloat(Buffer.concat([buffer,new Buffer("00","hex")]));break;
        	case 8 : num = readInt64LEasFloat(buffer);break;
        	case 9 : num = readInt64LEasFloat(buffer);break;

        }
        return num;

	}

}


function readInt64LEasFloat(buffer){
	var low = buffer.readInt32LE(0);
	var n = buffer.readInt32LE(4) * 4294967296.0 + low;
	if (low < 0) n += 4294967296;

	return n;
}