var test = require('./numbers');
var assert = require('assert');
var varint = require('../index');

describe('GetVarint64', function() {
  describe('#dont fail on empty buffer', function() {
    it('should return 0', function() {
		var buffer = new Buffer("");
		var pResult = new Buffer(9);
        var res = varint.GetVarint64(buffer,buffer.length,pResult);
        assert.equal(res, 0);
    });
  }),

   Object.keys(test).forEach(function(_t){
   	describe("#decode known encoded "+_t+" bit buffer",function(){
	  	it('should return the decoded number '+test[_t].number,function(){
	  		var buffer = new Buffer(test[_t].encoded,"hex");
			var pResult = new Buffer(9);
	        var res = varint.GetVarint64(buffer,buffer.length,pResult);
	        assert.equal(test[_t].number, varint.toNumber(pResult.slice(0,res)));

	  	})
	  });
   })
  
  describe("#decode known encoded timetamp",function(){
  	it('should return the decoded unix timestamp',function(){
  		var buffer = new Buffer("fb58992636","hex");
		var pResult = new Buffer(9);

        var res = varint.GetVarint64(buffer,buffer.length,pResult);
        var expected = "1486431798";
        assert.equal(expected, varint.toNumber(pResult.slice(0,res)));

  	})
  });
});
