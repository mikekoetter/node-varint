var test = require('./numbers');

var assert = require('assert');
var varint = require('../index');
describe('PutVarint64', function() {
  describe('#dont fail on empty buffer', function() {
    it('should return 0', function() {
		var buffer = new Buffer("");
        var res = varint.PutVarint64(buffer,0);
        assert.equal(res, 0);
    });
  })

   Object.keys(test).forEach(function(_t){
    describe("#encode "+_t+" bit buffer",function(){
      it('should fill the suppled buffer with encoded data: '+test[_t].encoded,function(){
        var pResult = new Buffer(9);
          var res = varint.PutVarint64(pResult,test[_t].number);
          assert.equal(pResult.slice(0,res).toString("hex"), test[_t].encoded);

      })
    });
   })

  describe('#encode timestamp 1486431798', function() {
    it('should fill the suppled buffer with an encoded data from timestamp', function() {
		var toEncode = 1486431798; // unix ts for 2017-02-06 17:43:18
		var pResult = new Buffer(9);
        var res = varint.PutVarint64(pResult,toEncode);
        var expectedHex = "fb58992636"
        assert.equal(pResult.slice(0,res).toString("hex"), expectedHex);
    });
  });


});
